from django.contrib import admin
from .models import Order, OrderProducts


class OrderProductsToolbar(admin.TabularInline):
    model = OrderProducts
    extra = 1
    fields = ('product', 'qty', )


class OrderAdmin(admin.ModelAdmin):
    readonly_fields = ['total_cost', 'shipping_cost', 'date_update', 'date_create']
    inlines = [OrderProductsToolbar]
    list_display = ['name']

admin.site.register(Order, OrderAdmin)
