from django.contrib import admin
from .models import Product, ProductBrand


class ProductAdmin(admin.ModelAdmin):
    list_display = ['name', 'price', 'available', 'delivery_period', 'qty']
    list_filter = ('available', )
    readonly_fields = ['price']
    search_fields = ['name', 'article']

admin.site.register(Product, ProductAdmin)
admin.site.register(ProductBrand)

