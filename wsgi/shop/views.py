from django.shortcuts import render
from .models import Product
from django.db.models import Q
from django.views.generic import ListView


class ProductList(ListView):
    model = Product
    paginate_by = 20

    def get_queryset(self):
        q = self.request.GET.get('q', None)
        queryset = super(ProductList, self).get_queryset()
        # queryset = Product.objects.get_queryset()

        if q is not None:
            queryset.filter(article__contains=q)
            return queryset

        return queryset