# -*- coding: utf-8 -*-
from django.apps import AppConfig


class CartConfig(AppConfig):
    name = 'cart'
    verbose_name = 'Магазин'
