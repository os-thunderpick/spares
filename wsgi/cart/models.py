# -*- coding: utf-8 -*-
import decimal

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.formats import date_format as dfmt

from shop.models import Product
from .signals import *


class Order(models.Model):
    id = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=255, editable=False, null=True, blank=True)
    user = models.ForeignKey(User)
    STATUSES = (
        (u"1", u"Ожидает обработки"),
        (u"2", u"В обработке"),
        (u"3", u"Готов к отправлению"),
        (u"4", u"Отправлен"),
        (u"5", u"Выполнен"),
        (u"6", u"Аннулирован"),
        (u"7", u"Ожидает поступления на склад"),
        # (u"", u"Ожидает оплаты"),
        # (u"", u"Оплачен"),
    )
    status = models.CharField(max_length=2, choices=STATUSES, verbose_name=u"Статус")
    date_create = models.DateTimeField(editable=True, default=timezone.now,
                                       verbose_name=u"Время добавления")
    date_update = models.DateTimeField(editable=True, default=timezone.now,
                                       verbose_name=u"Время обновления")
    total_cost = models.DecimalField(decimal_places=2, max_digits=10, default=0, verbose_name=u"Итого по заказу")
    shipping_cost = models.BigIntegerField(verbose_name=u"Доставка", default=250)

    def save(self):
        """
        Считаем сумму за товар и доставку, устанавливаем название
        """

        """
        Считаем сумму за товар
        """
        tmp_total = 0
        products_in_order = self.orderproducts_set.all()
        for product in products_in_order:
            tmp_total = tmp_total + decimal.Decimal(product.product.price) * product.qty
        """
        Считаем цену доставки
        """
        if int(tmp_total) > 5000:
            self.shipping_cost = 0

        self.total_cost = tmp_total + self.shipping_cost
        """
        Устанавливаем название
        """
        self.name = u' '.join([
            u'Заказ для ',
            self.user.username,
            u' от ',
            dfmt(self.date_create, format='DATETIME_FORMAT', use_l10n=True),
            u' изменен [ ',
            dfmt(self.date_update, format='DATETIME_FORMAT', use_l10n=True),
            u' ]'
        ])
        """
        Обновляем даты
        """
        self.date_update = timezone.now()
        """
        Сохраняем
        """
        super(Order, self).save()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u"Заказ"
        verbose_name_plural = u"Заказы"


models.signals.post_save.connect(order_created, sender=Order)
models.signals.post_delete.connect(order_deleted, sender=Order)


class OrderProducts(models.Model):
    product = models.ForeignKey(Product)
    order = models.ForeignKey(Order)
    qty = models.PositiveIntegerField(default=1, verbose_name=u"Количество")
    sale = models.IntegerField(default=0, verbose_name=u"Скидка")

    def __str__(self):
        return u'Товар в заказе'

    def save(self, force_insert=False, force_update=False, using=None):
        super(OrderProducts, self).save(force_insert=force_insert, force_update=force_update, using=using)
        self.order.save()

    def delete(self, using=None):
        super(OrderProducts, self).delete(using=using)
        self.order.save()

    class Meta:
        verbose_name = u"товар в заказ"
        verbose_name_plural = u"Товары в заказе"
