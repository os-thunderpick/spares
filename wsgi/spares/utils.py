# -*- coding: utf-8 -*-
import os
from hashlib import md5


def upload_to(instance, filename, folder='upload'):
    hex = md5(filename).hexdigest()
    file_root, file_ext = os.path.splitext(filename)
    return '/'.join([folder, hex[:3], hex[3:6], hex]) + file_ext