# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-04-22 15:43
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('shop', '0002_auto_20160422_1820'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='delivery_period',
            field=models.SmallIntegerField(default=0, verbose_name='Срок доставки(дней)'),
        ),
        migrations.AddField(
            model_name='product',
            name='is_available',
            field=models.BooleanField(default=True, verbose_name='Наличие'),
        ),
        migrations.AddField(
            model_name='product',
            name='markup',
            field=models.PositiveSmallIntegerField(default=25, verbose_name='Наценка'),
        ),
    ]
