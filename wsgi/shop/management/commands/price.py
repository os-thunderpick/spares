import csv
import decimal
import logging
from django.core.management.base import BaseCommand
from django.db.utils import DataError

from shop.models import Product, ProductBrand

logger = logging.getLogger('terminal')

class Command(BaseCommand):
    help = 'Load price from CSV'

    fields_map = {
        'brand': 0,
        'code': 1,
        'name': 2,
        'qty': 3,
        'price_per': 4,
        'price': 5,
        'delivery_period': 6
    }

    def add_arguments(self, parser):
        parser.add_argument('file_path', nargs='+', type=str)

    def handle(self, *args, **options):

        for file in options['file_path']:
            with open(file, encoding='cp1251') as price:
                reader = csv.reader(price, delimiter=';')
                reader = iter(reader)
                next(reader) # skip first line that contains column names

                for i, row in enumerate(reader):
                    brand = None
                    if row[self.fields_map['brand']] is not None:
                        brand, b_created = ProductBrand.objects.get_or_create(name=row[self.fields_map['brand']])
                        msg = 'Get brand {0} from DB'
                        if b_created:
                            msg = 'Added new brand {0}'
                        logger.info(msg.format(brand.name))

                    defaults = {
                        'brand': brand,
                        'article': row[self.fields_map['code']],
                        'name': row[self.fields_map['name']],
                        'price_opt': decimal.Decimal(str(row[self.fields_map['price']]).replace(',', '.')),
                        'delivery_period': int(str(row[self.fields_map['delivery_period']]).replace(' день', '')
                                               .replace(' дня', '')
                                               .replace(' дней', '')),
                        'available': 'AV'
                    }
                    qty = int(str(row[self.fields_map['qty']]).strip(' '))

                    if qty < 1:
                        defaults['available'] = 'DAV'
                        qty = 0

                    defaults['qty'] = qty

                    try:

                        product, p_created = Product.objects.update_or_create(article=row[self.fields_map['code']],
                                                                           defaults=defaults)

                        msg = 'Update product {0} [{2}] {1}'
                        if p_created:
                            msg = 'Added new product {0} [{2}] {1}'
                        logger.info(msg.format(product.id, product.name, product.available))
                    except DataError as e:
                        logger.error('Error with product at line: {1} - {0} [{2}] - {3}'.format(e, i, qty, defaults['available']))
                        continue
