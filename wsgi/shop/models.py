# -*- coding: utf-8 -*-
from django.db import models
from decimal import *


class ProductBrand(models.Model):
    name = models.CharField(max_length=2000, verbose_name=u'Бренд')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = u'бренд'
        verbose_name_plural = u'бренды'


class ProductManager(models.Manager):
    def has_no_brand(self):
        return super(ProductManager, self).get_queryset().filter(brand__name__exact=None)


class Product(models.Model):
    objects = ProductManager()

    # Внешний ID - служит для связи импортируемого товара с нашим ID
    external_id = models.CharField(max_length=2000, verbose_name=u'Внешний ID', null=True, blank=True)
    # Основная инфа
    name = models.CharField(max_length=2000, verbose_name=u'Торговое название')
    brand = models.ForeignKey(ProductBrand, null=True, blank=True, verbose_name=u'Бренд')
    article = models.CharField(max_length=2000, verbose_name=u'Артикул')
    description = models.TextField(verbose_name=u'Описание')
    # Цены
    price = models.DecimalField(decimal_places=2, max_digits=10, default=0, verbose_name=u"Цена")
    price_opt = models.DecimalField(decimal_places=2, max_digits=20, verbose_name=u'Закупочная цена', default=0.00)
    markup = models.PositiveIntegerField(default=25, verbose_name=u'Наценка')
    # Наличие и доставка
    A_OPTIONS = (
        ('AV', u'В наличии',),
        ('DAV', u'Нет в ниличии',),
        ('PRE', u'На заказ',),
        ('ON', u'Только онлайн заказ',),
    )
    available = models.CharField(choices=A_OPTIONS, default='AV', max_length=3, verbose_name=u"Наличие товара")
    delivery_period = models.SmallIntegerField(default=0, verbose_name=u'Срок доставки(дней)')
    qty = models.IntegerField(verbose_name=u'Остаток', default=1)

    def __unicode__(self):
        return self.name

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        self.price = self.price_opt*Decimal('1.{0}'.format(self.markup))
        super(Product, self).save(force_insert=force_insert, force_update=force_update, using=using, update_fields=update_fields)

    class Meta:
        verbose_name = u'товар'
        verbose_name_plural = u'товары'