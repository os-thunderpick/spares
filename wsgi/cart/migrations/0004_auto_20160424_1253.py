# -*- coding: utf-8 -*-
# Generated by Django 1.9.3 on 2016-04-24 09:53
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cart', '0003_auto_20160422_1951'),
    ]

    operations = [
        migrations.AlterField(
            model_name='orderproducts',
            name='sale',
            field=models.IntegerField(default=0, verbose_name='Скидка'),
        ),
    ]
