from django.conf.urls import url
from .views import ProductList

urlpatterns = [
    'shop.views',
    url(r'^$', ProductList.as_view(), name='list')
]